import numpy as np
import matplotlib.pyplot as plt
import random
import copy
import timeit

# Implemented by Mohit Kumra found on https://www.geeksforgeeks.org/insertion-sort/
def insertion_sort(arr):
    for i in range(1, len(arr)):
        key = arr[i]
        j = i-1
        while j >= 0 and key < arr[j] :
                arr[j + 1] = arr[j]
                j -= 1
        arr[j + 1] = key


# Implemented by Mohit Kumra found on https://www.geeksforgeeks.org/python-program-for-merge-sort/
def merge(arr, l, m, r):
    n1 = m - l + 1
    n2 = r - m
 
    # create temp arrays
    L = [0] * (n1)
    R = [0] * (n2)
 
    # Copy data to temp arrays L[] and R[]
    for i in range(0, n1):
        L[i] = arr[l + i]
 
    for j in range(0, n2):
        R[j] = arr[m + 1 + j]
 
    # Merge the temp arrays back into arr[l..r]
    i = 0     # Initial index of first subarray
    j = 0     # Initial index of second subarray
    k = l     # Initial index of merged subarray
 
    while i < n1 and j < n2:
        if L[i] <= R[j]:
            arr[k] = L[i]
            i += 1
        else:
            arr[k] = R[j]
            j += 1
        k += 1
 
    # Copy the remaining elements of L[], if there
    # are any
    while i < n1:
        arr[k] = L[i]
        i += 1
        k += 1
 
    # Copy the remaining elements of R[], if there
    # are any
    while j < n2:
        arr[k] = R[j]
        j += 1
        k += 1
 
# l is for left index and r is right index of the
# sub-array of arr to be sorted
 
 
def mergeSort(arr, l, r):
    if l < r:
 
        # Same as (l+r)//2, but avoids overflow for
        # large l and h
        m = l+(r-l)//2
 
        # Sort first and second halves
        mergeSort(arr, l, m)
        mergeSort(arr, m+1, r)
        merge(arr, l, m, r)


def sort_comparison(n, trials):
    data = np.zeros((trials, 2))
    insert_total = 0
    merge_total = 0
    for t in range(trials):
        arr1 = np.random.randint(0, n, n)
        arr2 = copy.deepcopy(arr1)

        insertion_sum = 0
        for i in range (5):
            start = timeit.default_timer()
            insertion_sort(arr1)
            end = timeit.default_timer()
            insertion_sum += end - start
        insertion_avg = insertion_sum/10
        #print(arr1)
        #print(arr2)
        merge_sum = 0
        for i in range (5):
            start = timeit.default_timer()
            mergeSort(arr2, 0, n-1)
            end = timeit.default_timer()
            merge_sum += end - start
        merge_avg = merge_sum/10
        insert_total += insertion_avg
        merge_total += merge_avg
    return (insert_total/trials, merge_total/trials)


def main():
    n = 400
    arr = np.zeros((n, 2))
    for i in range(1,n):
        arr[i] = sort_comparison(i, 10)
    print(arr)
    plt.plot(arr)
    plt.legend(['Insertion Sort', 'Merge Sort'])
    plt.ylabel("Time")
    plt.xlabel("Array Length")
    plt.savefig("plot1")
    

if __name__ == "__main__":
    main()    